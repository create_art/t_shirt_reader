import logging
from t_reader.read_shirt import ascii_shirt
from t_reader.read_shirt import read_shirt, block_ascii, binary_to_ascii

TEST_IMAGE = "tests/data/aws_ascii_binary.jpg"

def test_read_shirt():
    str_res = read_shirt(TEST_IMAGE)
    assert str_res is not None
    assert "01" in str_res


def test_ascii_blocks():
    line_data = open("tests/data/aws_ascii_unblocked.txt").readlines()
    test_data = "\n".join(line_data)
    ascii_content = block_ascii(test_data)
    assert ascii_content is not None
    assert len(ascii_content) == 12


def test_binary_text():
    text = binary_to_ascii("01010111011010000110000101110100")
    assert text is not None
    assert "What" in text

    text = binary_to_ascii("01010111 01101000 01100001 01110100 01110011 00100000 01100100 0100001 01101110 01100111 01100101110010")
    assert text is None

def test_hand_decode(manual_str):
    text = binary_to_ascii(manual_str)
    assert text == "Whats dangerous is not to ev"

def test_ascii_shirt():
    text = ascii_shirt(TEST_IMAGE)
    logging.info(text)
    assert text is not None
    assert 'What' in text
    assert text == 'What'
