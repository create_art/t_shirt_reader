from tokenize import String
from PIL import Image
import binascii

import pytesseract
import logging

pytesseract.pytesseract.tesseract_cmd = r"C:\Program Files\Tesseract-OCR\tesseract.exe"


def ascii_shirt(image_file: str):
    unblocked_text = read_shirt(image_file)
    logging.info(unblocked_text)
    block_text = block_ascii(unblocked_text)
    logging.info(block_text)
    all_text = "".join(block_text)
    return binary_to_ascii(all_text)


def read_shirt(image_file: str):
    cfg_file = 'binary'
    return pytesseract.image_to_string(Image.open(image_file),config=cfg_file)


def block_ascii(ocr_ascii: str):
    """
    Accumulate 0 or 1 character into blocks of 8
    skip spaces and drop newlines this should fail
    near the bottom of the shirt
    """
    all_blocks = []
    long_string = ocr_ascii.replace("\n", "")
    cur_block = ""
    for character in long_string:
        if character in ["0", "1"]:
            cur_block += character
        else:
            logging.info(character)
        if character == " " or character == "\n":
            if len(cur_block) == 8:
                all_blocks.append(cur_block)
            cur_block = ""
    #out of characters, append last block as is, off by 1 error
    if len(cur_block) == 8:
        all_blocks.append(cur_block)
    return all_blocks


def binary_to_ascii(input_binary: str):
    input_binary = input_binary.replace(' ','')
    num = int("0b" + input_binary, 2)
    try:
        text = binascii.unhexlify("%x" % num)
    except binascii.Error:
        return
    return text.decode()
