from flask import Flask


def create_app():
    app = Flask("T-Shirt Reader")

    @app.route("/")
    def get_default():
        return "T-Shirt Reader App"

    return app


if __name__ == "__main__":
    app = create_app()
    app.run()
