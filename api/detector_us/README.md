# T-Shirt Detector Microservice

Identifies if a given image contains a t-shirt or not.
Based on YOLO training data : https://universe.roboflow.com/yolo5tshirt/tshirt_detection
Trained using custom dataset method : https://github.com/ultralytics/yolov5/wiki/Train-Custom-Data
YOLOv5 is fairly political : https://curiousily.com/posts/object-detection-on-custom-dataset-with-yolo-v5-using-pytorch-and-python/
