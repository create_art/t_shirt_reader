# Multi Technology Infracodes Demo

This repository demonstrates the myriads in ways a 2-tier serverless application infrastructure can be provisioned. There is little, to-no persistence.
It operates in true web native stateless / Dory like mode, other than the analytics trackers you may choose to install to see how many eye balls your
idea garners.

## Roadmap
- Pulumi
- Terraform
- Cloudformation
- CDK
- ARM Templates
- 3-Tier

# AWS Services Used
![AWS Services](./docs/services_used.png)

## Cloud Agnostic

### Pulumi
https://github.com/pulumi/examples/tree/master/aws-ts-static-website

### Terraform
https://github.com/chgangaraju/terraform-aws-cloudfront-s3-website

## Cloud Specific

### ARM Templates

### Cloudformation