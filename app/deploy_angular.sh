#!/bin/bash
export $(cat .env | xargs)
cd t-shirt-reader-web || exit
ng build t-shirt-reader -c production
cd dist/t-shirt-reader || exit
aws s3 sync . s3://"$S3_BUCKET"
aws cloudfront create-invalidation --distribution-id "$CF_DISTRO" --paths "/*"