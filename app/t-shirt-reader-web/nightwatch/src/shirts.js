describe('shirts reader submit image', function() {
    const snapButton = element('[class="btn btn-success"]');

    it('should read a t-shirt image', function() {
        browser
        .navigateTo('https://shirts.whatnick.org')
        .click(snapButton)
        .expect.elements('[class="shirt-text"]').count.to.equal(1);
    });

    after(browser => browser.end());
});