import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LanguageChoiceComponent } from './language-choice.component';

describe('LanguageChoiceComponent', () => {
  let component: LanguageChoiceComponent;
  let fixture: ComponentFixture<LanguageChoiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LanguageChoiceComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LanguageChoiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
