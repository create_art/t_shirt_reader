import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-shirt-text',
  templateUrl: './shirt-text.component.html',
  styleUrls: ['./shirt-text.component.sass']
})
export class ShirtTextComponent implements OnInit {

  @Input() content = '';

  constructor() { }

  ngOnInit(): void {
  }

}
