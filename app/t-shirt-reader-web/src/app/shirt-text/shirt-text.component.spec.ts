import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShirtTextComponent } from './shirt-text.component';

describe('ShirtTextComponent', () => {
  let component: ShirtTextComponent;
  let fixture: ComponentFixture<ShirtTextComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShirtTextComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ShirtTextComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
