import { Component, OnInit } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { WebcamImage, WebcamInitError, WebcamUtil } from 'ngx-webcam';
  
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {
  
    title = 't-shirt-reader';
    private trigger: Subject<unknown> = new Subject();

    public webcamImage!: WebcamImage;
    private nextWebcam: Subject<unknown> = new Subject();

    captureImage  = '';
    currentText = '';
  
    ngOnInit() {}
  
    /*------------------------------------------
    --------------------------------------------
    triggerSnapshot()
    --------------------------------------------
    --------------------------------------------*/
    public triggerSnapshot(): void {
        var v;
        this.trigger.next(v);
        console.info("Taking snapshot")
    }
  
    /*------------------------------------------
    --------------------------------------------
    handleImage()
    --------------------------------------------
    --------------------------------------------*/
    public handleImage(webcamImage: WebcamImage): void {
        this.webcamImage = webcamImage;
        this.captureImage = webcamImage!.imageAsDataUrl;
        this.currentText = "Uncle Jeff gave me this Shirt"
        console.info('received webcam image', this.captureImage);
    }
  
    /*------------------------------------------
    --------------------------------------------
    triggerObservable()
    --------------------------------------------
    --------------------------------------------*/
    public get triggerObservable(): Observable<any> {

        return this.trigger.asObservable();
    }
  
    /*------------------------------------------
    --------------------------------------------
    nextWebcamObservable()
    --------------------------------------------
    --------------------------------------------*/
    public get nextWebcamObservable(): Observable<any> {

        return this.nextWebcam.asObservable();
    }
 
}