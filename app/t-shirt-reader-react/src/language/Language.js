import React from "react";

const LangSelect = () => (
<select>
	<option value="32">Arabic </option>
	<option value="47">Bahasa Indonesia</option>
	<option value="43">Chinese</option>
	<option value="8">Czech</option>
	<option value="9">Danish</option>
	<option value="10">Dutch</option>
	<option value="23">Dutch (Belgium)</option>
	<option selected="selected" value="22">English (Australia)</option>
	<option value="5">English (Canada)</option>
	<option value="21">English (South Africa)</option>
	<option value="4">English (UK)</option>
	<option value="1">English (US)</option>
	<option value="11">Finnish</option>
	<option value="24">French (Belgium)</option>
	<option value="25">French (Canada)</option>
	<option value="2">French (France)</option>
	<option value="12">German</option>
	<option value="14">Italian</option>
	<option value="15">Norwegian</option>
	<option value="16">Polish</option>
	<option value="7">Portuguese (Brazil)</option>
	<option value="17">Portuguese (European)</option>
	<option value="33">Romanian</option>
	<option value="35">Russian (Russia)</option>
	<option value="6">Spanish (Europe)</option>
	<option value="3">Spanish (International)</option>
	<option value="18">Swedish</option>
	<option value="44">Thai</option>
	<option value="19">Turkish</option>
</select>
);

export default LangSelect