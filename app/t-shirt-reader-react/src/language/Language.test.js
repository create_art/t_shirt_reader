import { render, screen } from '@testing-library/react';
import LangSelect from './Language';

test('renders select box', () => {
    render(<LangSelect />);
    const selectElement = screen.getByText(/english (australia)/i);
    expect(selectElement).toBeInTheDocument();
  });